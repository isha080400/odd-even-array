import java.util.Scanner;

public class OddEvenArrayFinder {

    public static int getOddEvenArray() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        int countOdd = 0, countEven = 0;

        for (int i = 0; i < n; i++) {
            if (arr[i] % 2 == 0) {
                countEven++;
            } else if (arr[i] % 2 != 0) {
                countOdd++;
            }
        }

        if (countEven == n) {
            return 1;
        } else if (countOdd == n) {
            return 2;
        } else {
            return 3;
        }
    }

    public static void main(String[] args) {
        int x = getOddEvenArray();

        if (x == 1) {
            System.out.println("Even");
        } else if (x == 2) {
            System.out.println("Odd");
        } else {
            System.out.println("Mixed");
        }
    }
}
